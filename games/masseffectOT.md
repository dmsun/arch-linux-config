## Mass Effect 1
### Install
ISO has been created, I can't recall how this went in Wine/Lutris. May have trouble.
Key is 5BVV-NUWG-AGX3-QCKW-CXY3
WineArch: 32-Bit

### Lutris
Ensure the Lutris configure executable points to the MassEffect.exe and not the Launcher. The Launcher has issues.

In prefix create file;
%PROGRAM FILES%/Origin Games/Mass Effect/MassEffectLauncher.exe

This is what the Patch Installer, and DLC installers look for...
https://answers.ea.com/t5/Mass-Effect-1/Information-Mass-Effect-1-and-Win7-or-Win8-You-need-patch-1-02/td-p/368322

### DLC
- https://help.ea.com/en-us/help/origin/origin/download-dlc-for-classic-origin-games/#masseffect
There are 2 DLCs, Bring Down the Sky (fun!) and Pinnacle Station(?)
The DLC can be downloaded at the above location. I don't think a DLC patcher is required for these.
Note: this downloads the executeables. To install;
#### Pinnacle Station
WINEPREFIX=/home/mj/Games/Lutris/mass-effect wine ./MassEffect_PS_ES.exe
Key: 5GUC-XRFP-QNBM-TJBD-3L5J
#### Bring Down the Sky
WINEPREFIX=/home/mj/Games/Lutris/mass-effect wine ./MassEffect_BDtS_EFIGS.exe
### Mods
#### Mass Effect Recalibrated
Fixes bugs
#### Faster Elevators
Self-explanatory - requires ME3Tweaks Mod Manager
#### MEUITM 
Textures Install After Above
#### ALOT
Install last
Textures
