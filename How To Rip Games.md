## PSX
1: Open Brasero
2: Select Data Copy
3: Select Properties - select filetype .CUE
4: Remove the filepath from the filename reference in the .CUE file. Include only the filename of the .BIN.
NB: If you edit the filename of the .BIN, you'll need to update the contents of the CUE
Test in Mednafen is successful.


## Creating Wineprefix and installing from Disc
1: WINEPREFIX="/home/mj/Games/Lutris/gamename" wine wineboot
2: WINEPREFIX="/home/mj/Games/Lutris/gamename" wine /run/media/mj/CDROMNAME/setup.exe
